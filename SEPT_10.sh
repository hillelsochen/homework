#!/bin/sh

DIR="/home/$USER/bin"
FILE="$DIR/hello_world.sh"

create_dir () {
  mkdir $DIR
}

create_file () {
  cat > $FILE << EOF
  #!/bin/sh
  printf "Hello World!"
  EOF
}

if [ test -d $DIR ]
then
  if [ test ! -f $FILE ]
  then
    create_file
  fi
else
  create_dir
  create_file
fi

printf "Hello $USER" >> $FILE